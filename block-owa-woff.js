// Returns { cancel: true } or { cancel: false }
// https://developer.chrome.com/extensions/webRequest#type-BlockingResponse
function isFontRequest(details) {
    const url = new URL(details.url)
    const path = url.pathname.toLowerCase()
    const cancel = (path.endsWith(".woff2") || path.endsWith(".woff") || path.endsWith(".ttf")) && path.indexOf("icons") == -1

    return { cancel }
}

const filter = {
    urls: [
        "*://*/owa*.woff*",
        "*://*/owa*.ttf*",
    ],
    types: ["font"],
}

// 'blocking' means we're not an async callback.  This allows us to cancel a request
const extraInfoSpec = ["blocking"]

chrome.webRequest.onBeforeRequest.addListener(isFontRequest, filter, extraInfoSpec)
